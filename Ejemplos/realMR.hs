import Klytius
import Klytius.PGrafo
import Strat
import Data.List.Split

simpleMapReduce
    :: Functor f
    => (a -> b)
    -> (f b -> c)
    -> f a
    -> c
simpleMapReduce mp red = red . fmap mp

mapReduce 
    :: Functor f
    => Strategy (f b)
    -> (a -> b)
    -> Strategy c
    -> (f b -> c)
    -> f a
    -> TPar c
mapReduce strfb mp strc red xs = seqs "mapRes->redRes" mapRes redRes
    where
        mapRes = fmap mp xs `using` strfb
        redRes = (red <$> mapRes) `rusing` strc

oneWord :: String -> (String, Int)
oneWord s = (s,1)

summ :: [(String,Int)] -> String -> Int -> [(String, Int)]
summ [] s i = [(s,i)]
summ ((s,i):xs) ns ni   | (s == ns) = (s,i+ni) : xs
                        | s < ns = (s,i) : (summ xs ns ni)
                        | otherwise = (ns, ni) : ((s,i):xs)

normalize
    :: [(String,Int)] -- vienen repetidos, o ("ss",1),("ss",2)...
    -> [(String,Int)] -- todo bien, ("ss",3).
normalize xs = foldl (\a (w,i) -> summ a w i) [] xs

contPal :: [String] -> [(String, Int)]
contPal xs = simpleMapReduce oneWord normalize xs

rightTup :: Strategy (String,Int)
rightTup (a,b) = mkVar (,) <*> (vlbl a (r0 a)) <*> (vlbl (show b) (rwhnf b))

parContPal :: [String] -> TPar [(String, Int)]
parContPal xs = mapReduce (parList rightTup) oneWord (parList rightTup) normalize xs

parContPalC :: [String] -> TPar [(String, Int)]
parContPalC xs = mapReduce (parListChunk 4 rightTup) oneWord (parListChunk 4 rightTup) normalize xs

ejemplo :: String
ejemplo = "contar saltar contar pegar"

ejemplo2 :: String
ejemplo2 = "Al fin el dia llego en que Martin va a dejar de molestar a los profesores."

main = do
    graficar False (parContPal (splitOn " " ejemplo)) "molestar"
    custy False True 0.25 (parContPalC (splitOn " " ejemplo2)) "chunks"
    putStrLn $ show $ exec (parContPal (splitOn " " ejemplo))
